import React, { Component } from 'react';
import './Todo.css';

class Todo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      task: this.props.task,
      editing: false,
      completed: false
    };
    this.handleDelete = this.handleDelete.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.toggleComplete = this.toggleComplete.bind(this)
  }
  handleDelete() {
    this.props.delete(this.props.id);
  }
  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }
  handleEdit(event) {
    this.setState(st => {
      let newEdit = !st.editing;
      return { editing: newEdit };
    });
  }
  toggleComplete(){
    this.setState({completed: !this.state.completed})
  }

  render() {
    return (
      <div className="Todo">
        {!this.state.editing ? (
          <div className="Todo-task-wrapper">
            <div className="Todo-task">
              <h1 className ={this.state.completed ? "Todo-completed":''}onClick={this.toggleComplete}>{this.state.task}</h1>
            </div>
            <div className="Todo-action">
              <button onClick={this.handleDelete}><i class="fas fa-trash"></i></button>
              <button onClick={this.handleEdit}><i class="fas fa-pen"></i></button>
            </div>
          </div>
        ) : (
          <form onSubmit={this.handleEdit} className = "Todo-edit">
            <input
              type="text"
              name="task"
              value={this.state.task}
              onChange={this.handleChange}
            />
            <button>Submit</button>
          </form>
        )}
      </div>
    );
  }
}

export default Todo;
