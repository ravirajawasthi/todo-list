import React, { Component } from 'react';
import Todo from './Todo';
import NewTodoForm from './NewTodoForm';

import './TodoList.css';

import uuid from 'uuid';

class TodoList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todos: []
    };
    this.newTodo = this.newTodo.bind(this);
    this.deleteTodo = this.deleteTodo.bind(this);
  }

  newTodo(task) {
    const id = uuid();
    const newTodo = (
      <Todo task={task} key={id} id={id} delete={this.deleteTodo} />
    );
    const newTodos = [...this.state.todos, newTodo];
    this.setState({ todos: newTodos });
  }

  deleteTodo(id) {
    let newTodos = this.state.todos.filter(todo => {
      return todo.props.id !== id;
    });
    this.setState({
      todos: newTodos
    });
  }

  render() {
    return (
      <div className="TodoList">
        <h1 className="TodoList-header">Todo list</h1>
        {this.state.todos}
        <NewTodoForm new={this.newTodo} />
      </div>
    );
  }
}
export default TodoList;
