import React, { Component } from 'react';
import './NewTodoForm.css'
class NewTodoForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      TodoForm: ""
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleNew = this.handleNew.bind(this)
  }

  handleChange(event){
    this.setState({[event.target.name]: event.target.value})
  }

  handleNew(event){
    event.preventDefault()
    this.props.new(this.state.TodoForm)
    this.setState({TodoForm: ""})
  }

  render(){
    return(
      <div>
        <form onSubmit = {this.handleNew} className = "NewTodoForm">
          <label htmlFor = "NewTodoForm"></label>
          <input type = "text" required placeholder = "Be Happy!" onChange = {this.handleChange} name = "TodoForm" value = {this.state.TodoForm}/>
          <button>Submit</button>
        </form>
      </div>
    )
  }
}

export default NewTodoForm